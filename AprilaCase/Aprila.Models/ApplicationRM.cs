﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aprila.Models
{
    public class ApplicationRM
    {
        public int CaseNumber { get; set; }
        public string Comment { get; set; }
        public string InternalNote { get; set; }
        public int CompanyOrgNumber { get; set; }
        public Guid KYCid { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Accepted { get; set; }
        public bool Active { get; set; }
        public string CaseHandlerUsername { get; set; }

        public ApplicationRM() { }

        public ApplicationRM(int caseNumber, string comment, string internalNote, int orgNumber, DateTime dateCreated, bool accepted, bool active)
        {
            CaseNumber = caseNumber;
            Comment = comment;
            InternalNote = internalNote;
            CompanyOrgNumber = orgNumber;
            DateCreated = dateCreated;
            Accepted = accepted;
            Active = active;
        }
    }
}
