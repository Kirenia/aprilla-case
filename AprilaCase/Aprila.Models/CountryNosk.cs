﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aprila.Models
{

    public class CountryNorsk
    {
 
        public Classificationitem[] classificationItems { get; set; }

  
    }

   
    public class Classificationitem
    {
        public string code { get; set; }
        public string parentCode { get; set; }
        public string level { get; set; }
        public string name { get; set; }
        public string shortName { get; set; }
        public string notes { get; set; }

       
    }

}

