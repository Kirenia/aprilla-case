﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Aprila.Models
{
    public class KYC
    {
        [Key]
        public Guid KYCId { get; set; }
        public DateTime DateCreated { get; set; }
        public Company Company { get; set; }
        public string ContactPerson { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }
        public bool SignatureRight { get; set; }
        public bool BelongNorway { get; set; }
        public string Country { get; set; }
        public string SelectedCountry { get; set; }
        public string AccountNumber { get; set; }
        public string Purpose { get; set; }
        public UInt64 MonthlyTurnover { get; set; }
        public bool PEP { get; set; }
        public ICollection<Application> Applications { get; set; } = new List<Application>();

        public KYC() { }

        public KYC(DateTime dateCreated, Company company, Application application, string contactPerson, string email, string phone, bool signatureRight, bool belongNorway, string country, string selectedCountry, string accountNumber, string purpose, UInt64 monthlyTurnover, bool pep)
        {
            DateCreated = dateCreated;
            Company = company;
            ContactPerson = contactPerson;
            Applications.Add(application);
            Email = email;
            Phone = phone;
            SignatureRight = signatureRight;
            BelongNorway = belongNorway;
            Country = country;
            SelectedCountry = selectedCountry;
            AccountNumber = accountNumber;
            Purpose = purpose;
            MonthlyTurnover = monthlyTurnover;
            PEP = pep;
        }
    }
}
