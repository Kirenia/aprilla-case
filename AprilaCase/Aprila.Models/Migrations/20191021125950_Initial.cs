﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Aprila.Models.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CaseHandlers",
                columns: table => new
                {
                    Username = table.Column<string>(nullable: false),
                    Firstname = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    Salt = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CaseHandlers", x => x.Username);
                });

            migrationBuilder.CreateTable(
                name: "Companies",
                columns: table => new
                {
                    OrgNumber = table.Column<int>(nullable: false),
                    OrgName = table.Column<string>(nullable: true),
                    Bankrupt = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Companies", x => x.OrgNumber);
                });

            migrationBuilder.CreateTable(
                name: "KYCs",
                columns: table => new
                {
                    KYCId = table.Column<Guid>(nullable: false),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    CompanyOrgNumber = table.Column<int>(nullable: true),
                    ContactPerson = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true),
                    SignatureRight = table.Column<bool>(nullable: false),
                    BelongNorway = table.Column<bool>(nullable: false),
                    Country = table.Column<string>(nullable: true),
                    SelectedCountry = table.Column<string>(nullable: true),
                    AccountNumber = table.Column<string>(nullable: true),
                    Purpose = table.Column<string>(nullable: true),
                    MonthlyTurnover = table.Column<decimal>(nullable: false),
                    PEP = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KYCs", x => x.KYCId);
                    table.ForeignKey(
                        name: "FK_KYCs_Companies_CompanyOrgNumber",
                        column: x => x.CompanyOrgNumber,
                        principalTable: "Companies",
                        principalColumn: "OrgNumber",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Applications",
                columns: table => new
                {
                    CaseNumber = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Comment = table.Column<string>(nullable: true),
                    InternalNote = table.Column<string>(nullable: true),
                    CompanyOrgNumber = table.Column<int>(nullable: true),
                    KYCId = table.Column<Guid>(nullable: true),
                    DateCreated = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Accepted = table.Column<bool>(nullable: false),
                    Active = table.Column<bool>(nullable: false),
                    Deleted = table.Column<bool>(nullable: false),
                    CaseHandlerUsername = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Applications", x => x.CaseNumber);
                    table.ForeignKey(
                        name: "FK_Applications_CaseHandlers_CaseHandlerUsername",
                        column: x => x.CaseHandlerUsername,
                        principalTable: "CaseHandlers",
                        principalColumn: "Username",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Applications_Companies_CompanyOrgNumber",
                        column: x => x.CompanyOrgNumber,
                        principalTable: "Companies",
                        principalColumn: "OrgNumber",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Applications_KYCs_KYCId",
                        column: x => x.KYCId,
                        principalTable: "KYCs",
                        principalColumn: "KYCId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Applications_CaseHandlerUsername",
                table: "Applications",
                column: "CaseHandlerUsername");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_CompanyOrgNumber",
                table: "Applications",
                column: "CompanyOrgNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Applications_KYCId",
                table: "Applications",
                column: "KYCId");

            migrationBuilder.CreateIndex(
                name: "IX_KYCs_CompanyOrgNumber",
                table: "KYCs",
                column: "CompanyOrgNumber");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Applications");

            migrationBuilder.DropTable(
                name: "CaseHandlers");

            migrationBuilder.DropTable(
                name: "KYCs");

            migrationBuilder.DropTable(
                name: "Companies");
        }
    }
}
