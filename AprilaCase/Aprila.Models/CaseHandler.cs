﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Security.Cryptography;
using System.Text;

namespace Aprila.Models
{
    public class CaseHandler
    {
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        [Key]
        public string Username { get; set; }
        public string Password { get; set; }
        public string Salt { get; set; }

        public ICollection<Application> Applications { get; set; }

        private string enteredPassword;


        public CaseHandler() { }

        public CaseHandler(string firstname, string lastname, string username, string password)
        {
            Firstname = firstname;
            Lastname = lastname;
            Username = username.ToLower();
            enteredPassword = password;
            HashPassword();
        }

        public void HashPassword()
        {
            // Making a new byte array to store salt
            byte[] salt;
            // Generate random salt and populate salt array
            new RNGCryptoServiceProvider().GetBytes(salt = new byte[16]);

            Salt = Convert.ToBase64String(salt);

            // Hash and salt it using PBKDF2
            // Hash and salt password using PBKDF2
            // Set to hash 10000 times
            var pbkdf2 = new Rfc2898DeriveBytes(enteredPassword, salt, 10000);

            // Place the string in the byte array (thats what getbytes does)
            // Place hashed string in a byte array
            byte[] hash = pbkdf2.GetBytes(20);

            // Make new byte array to store hashed password + salt
            // 36 long because 20 bytes are the hash and 16 bytes are the salt
            byte[] hashBytes = new byte[36];

            // place the hash and salt in their respective places
            Array.Copy(salt, 0, hashBytes, 0, 16);
            Array.Copy(hash, 0, hashBytes, 16, 20);

            // Convert combined salt + hash to a string and store this inside password
            Password = Convert.ToBase64String(hashBytes);

        }
        public bool validatePassword()
        {
            string savedPasswordHash = "";

            byte[] hashBytes = Convert.FromBase64String(savedPasswordHash);

            byte[] salt = new byte[16];

            Array.Copy(hashBytes, 0, salt, 0, 16);

            var pbkdf2 = new Rfc2898DeriveBytes("", salt, 10000);

            byte[] hash = pbkdf2.GetBytes(20);

            for (int i = 0; i < 20; i++)
            {
                if (hashBytes[i + 16] != hash[i])
                    return false;
            }

            return true;
        }
    }
}
