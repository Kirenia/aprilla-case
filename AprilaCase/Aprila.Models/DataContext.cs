﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using Microsoft.EntityFrameworkCore.Design;
using System;
using System.Collections.Generic;
using System.Text;

namespace Aprila.Models
{
    public class DataContext : DbContext
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<Application> Applications { get; set; }
        public DbSet<CaseHandler> CaseHandlers { get; set; }
        public DbSet<KYC> KYCs { get; set; }

        public DataContext(DbContextOptions<DataContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Company>()
                .HasMany(c => c.Applications)
                .WithOne(a => a.Company);
            modelBuilder.Entity<Company>()
                .HasMany(c => c.KYCs)
                .WithOne(k => k.Company);

            modelBuilder.Entity<Company>()
                .Property(c => c.OrgNumber)
                .ValueGeneratedNever();


            modelBuilder.Entity<Application>()
                .Property(a => a.DateCreated)
                .HasColumnType("datetime2");


            modelBuilder.Entity<CaseHandler>()
                .Property(ch => ch.Username)
                .ValueGeneratedNever();

            modelBuilder.Entity<CaseHandler>()
                .HasMany(ch => ch.Applications)
                .WithOne(a => a.CaseHandler);


            modelBuilder.Entity<KYC>()
                .HasMany(k => k.Applications)
                .WithOne(a => a.KYC);
        }

        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer(@"Data Source=(LocalDb)\MSSQLLocalDB;Initial Catalog=AprilaLocalDB;Integrated Security=SSPI;");
        //}
    }
}
