﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Aprila.Models
{
    public class Application
    {
        [Key]
        public int CaseNumber { get; set; }
        public string Comment { get; set; }
        public string InternalNote { get; set; }
        public Company Company { get; set; }
        public KYC KYC { get; set; }
        public DateTime DateCreated { get; set; }
        public bool Accepted { get; set; }
        public bool Active { get; set; }
        public bool Deleted { get; set; }
        public CaseHandler CaseHandler { get; set; }

        public Application() { }

        public Application(int caseNumber, string comment, string internalNote, Company company, DateTime dateCreated, bool accepted, bool active)
        {
            CaseNumber = caseNumber;
            Comment = comment;
            InternalNote = internalNote;
            Company = company;
            DateCreated = dateCreated;
            Accepted = accepted;
            Active = active;
        }

        public override string ToString()
        {
            return $"{CaseNumber}, '{Comment}', note: '{InternalNote}', accepted: {Accepted}, active: {Active}";
        }
    }
}
