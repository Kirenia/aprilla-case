﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Aprila.Models
{
    public class KYCRM
    {
        public Guid KYCId { get; set; }
        public DateTime DateCreated { get; set; }
        public int CompanyOrgNumber { get; set; }
        public string ContactPerson { get; set; }        
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool SignatureRight { get; set; }
        public bool BelongNorway { get; set; }
        public string Country { get; set; }
        public string SelectedCountry { get; set; }
        public string AccountNumber { get; set; }
        public string Purpose { get; set; }
        public UInt64 MonthlyTurnover { get; set; }
        public bool PEP { get; set; }

        public KYCRM()
        {
        }

        public KYCRM(DateTime dateCreated, int orgNumber, string contactPerson, string email, string phone, bool signatureRight, bool belongNorway, string country, string selectedCountry, string accountNumber, string purpose, UInt64 monthlyTurnover, bool pep)
        {
            DateCreated = dateCreated;
            CompanyOrgNumber = orgNumber;
            ContactPerson = contactPerson;
            Email = email;
            Phone = phone;
            SignatureRight = signatureRight;
            BelongNorway = belongNorway;
            Country = country;
            SelectedCountry = selectedCountry;
            AccountNumber = accountNumber;
            Purpose = purpose;
            MonthlyTurnover = monthlyTurnover;
            PEP = pep;
        }
    }
}
