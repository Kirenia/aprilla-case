﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Aprila.Models
{
    public class Company
    {
        [Key]
        public int OrgNumber { get; set; }
        public string OrgName { get; set; }
        public bool Bankrupt { get; set; }
        public ICollection<Application> Applications { get; set; } = new List<Application>();
        public ICollection<KYC> KYCs { get; set; } = new List<KYC>();

        public Company() { }
        public Company(int orgNumber, string orgName, bool bankrupt)
        {
            OrgNumber = orgNumber;
            OrgName = orgName;
            Bankrupt = bankrupt;
        }
    }
}
