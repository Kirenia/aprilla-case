﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AprilaCase.Data
{
    public class AppState
    {
        public int? orgNo { get; private set; }
        public int? caseNo { get; private set; }

        public event Action OnChange;

        public void SetOrgNo(int? value)
        {
            this.orgNo = value;
            NotifyStateChanged();
        }

        public void SetCaseNo(int? value)
        {
            this.caseNo = value;
            NotifyStateChanged();
        }

        private void NotifyStateChanged() => OnChange?.Invoke();
    }
}
