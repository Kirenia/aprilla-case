﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components.Authorization;

namespace AprilaCase.Services
{
    public class CustomAuthStateProvider : AuthenticationStateProvider
    {
        /// <summary>
        /// Overrides the AuthenticationState.GetAuthenticationstateAsync method, and always returns a hard-coded authenticated user
        /// </summary>
        /// <returns></returns>
        public override Task<AuthenticationState> GetAuthenticationStateAsync()
        {
            var identity = new ClaimsIdentity(new[]
            {
                new Claim(ClaimTypes.Name, "mrfibuli"),
            }, "Fake authentication type");

            var user = new ClaimsPrincipal(identity);

            return Task.FromResult(new AuthenticationState(user));
        }
    }

}
