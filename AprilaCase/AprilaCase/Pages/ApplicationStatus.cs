﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using AprilaCase.Data;
using Aprila.Models;
using System.Net.Http;
using Newtonsoft.Json;

namespace AprilaCase.Pages
{
   
    public class ApplicationStatusBase : ComponentBase
    {


        public ApplicationRM Result;
        //[Parameter] public string companyNumber { get; set; }
        public string status { get; set; }
        // public string messageTest { get; set; }

        public int? ApplicationNumber { get; set; }
        public bool loadFailed;
        [Inject]
        AppState appState { get; set; }

        ILogger<ApplicationRM> Logger { get; set; }

                              
        protected override async Task OnInitializedAsync()
        {


            using (var client = new HttpClient())
            {
                //Get the Case Number from appState Container

                if (appState.caseNo != null)
                {
                    ApplicationNumber = appState.caseNo;


                    try
                    {
                        loadFailed = false;

                        //Get a response from the Api url + specific case Number
                        HttpResponseMessage getApplication = await client.GetAsync("https://localhost:44399/api/Applications/" + ApplicationNumber);
                        getApplication.EnsureSuccessStatusCode();
                        //Get the response content
                        var response = await getApplication.Content.ReadAsStringAsync();

                        Result = JsonConvert.DeserializeObject<ApplicationRM>(response, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                        GetStatus();

                    }
                    catch (Exception ex)
                    {
                        loadFailed = true;
                        Logger.LogWarning(ex, "Failed to load application {ApplicationNumber}", ApplicationNumber);

                    }
                }


            }

        }


        private void GetStatus()


        {
            if (Result != null)
            {

                {
                    if (Result.Active)
                    {
                        status = ("Søknad under behandling");

                    }
                    else
                    {
                        if (Result.Accepted)
                        {

                            status = "Søknad godkjent";
                        }
                        else
                        {

                            status = "Søknad avvist";
                        }

                    }

                }



            }


        }
    }
}
