﻿using Microsoft.AspNetCore.Components;
using System;
using System.Threading.Tasks;
using AprilaCase.Data;
using System.Net.Http;
using Aprila.Models;
using Newtonsoft.Json;
using Blazored.LocalStorage;

namespace AprilaCase.Pages
{
    public class CustomerLoginBase : ComponentBase
    {
        [Inject]
        NavigationManager NavigationManager { get; set; }
        [Inject]
        AppState appState { get; set; }

        [Inject]
        ILocalStorageService localStorage { get; set; }

        protected ApplicationRM Result;
        protected CustomerLoginModel customerLogin = new CustomerLoginModel();
        protected bool errorFlag;
        protected string errorMessage;

        /// <summary>
        /// Fills the input fields with previously stored orgNo and caseNo, if any are present in localStorage
        /// </summary>
        /// <returns></returns>
        protected override async Task OnInitializedAsync()
        {
            ClearError();
            int? orgNoTemp = null;
            int? caseNoTemp = null;

            try
            {
                orgNoTemp = await localStorage.GetItemAsync<int?>("currentOrgNo");
                caseNoTemp = await localStorage.GetItemAsync<int?>("currentCaseNo");
            }
            catch (Exception e)
            {
                Console.WriteLine("Cannot load orgNo/caseNo saved: " + e.Message);
            }
            if (orgNoTemp != null)
            {
                customerLogin.orgNo = orgNoTemp;
            }
            if (caseNoTemp != null)
            {
                customerLogin.caseNo = caseNoTemp;
            }
        }

        /// <summary>
        /// Clears inpur fields and error message
        /// </summary>
        /// <returns></returns>
        protected async Task ClearClick()
        {
            ClearError();
            customerLogin.orgNo = null;
            customerLogin.caseNo = null;
        }

        /// <summary>
        /// If a valid submit type is entered, fetch the corresponding case
        /// </summary>
        /// <returns></returns>
        protected async Task HandleValidSubmit()
        {
            Console.WriteLine("OnValidSubmit");

            using (var client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage getApplication = await client.
                    GetAsync("https://localhost:44399/api/companies/" + customerLogin.orgNo + "/applications/" + customerLogin.caseNo);
                    if ((int)getApplication.StatusCode == 404)
                    {
                        Console.WriteLine(getApplication.StatusCode + ": Could not find this case");
                        errorMessage = "Det finnes ingen søknader for denne bedriften med dette saksnummeret. ";
                        errorFlag = true;
                        return;
                    }
                    if ((int)getApplication.StatusCode == 500)
                    {
                        Console.WriteLine(getApplication.StatusCode + ": Oops, something went wrong");
                        errorMessage = "Det finnes ingen søknader for denne bedriften med dette saksnummeret. ";
                        errorFlag = true;
                        return;
                    }
                    getApplication.EnsureSuccessStatusCode();
                    var response = await getApplication.Content.ReadAsStringAsync();
                    Result = JsonConvert.DeserializeObject<ApplicationRM>(response, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                    if ((int)getApplication.StatusCode == 200)
                    {
                        Console.WriteLine("OnValidCase");
                        ClearError();

                        await localStorage.SetItemAsync("currentOrgNo", customerLogin.orgNo);
                        await localStorage.SetItemAsync("currentCaseNo", customerLogin.caseNo);

                        appState.SetOrgNo(customerLogin.orgNo);
                        appState.SetCaseNo(customerLogin.caseNo);
                        NavigationManager.NavigateTo("ApplicationStatus/");
                        return;
                    }
                    else
                    {
                        errorMessage = "Det finnes ingen søknader for denne bedriften med dette saksnummeret. ";
                        errorFlag = true;
                    }
                }

                catch (Exception e)
                {
                    errorMessage = "Oida, noe gikk galt. ";
                    errorFlag = true;
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
        }

        /// <summary>
        /// Handles invalid submit
        /// </summary>
        protected void HandleInvalidSubmit()
        {
            Console.WriteLine("OnInvalidSubmit");
        }

        /// <summary>
        /// Clears error mesage and status
        /// </summary>
        protected void ClearError()
        {
            errorMessage = "";
            errorFlag = false;
        }
    }
}