﻿using Microsoft.AspNetCore.Components;
using System;
using System.Threading.Tasks;
using Aprila.Models;
using AprilaCase.Models;
using Newtonsoft.Json;
using System.Security.Cryptography;
using System.Net.Http;
using Blazored.LocalStorage;

namespace AprilaCase.Pages
{
    public class CaseHandlerLoginBase : ComponentBase
    {
        [Inject]
        NavigationManager NavigationManager { get; set; }

        [Inject]
        ILocalStorageService localStorage { get; set; }


        //[CascadingParameter]
        //protected Task<AuthenticationState> authenticationStateTask { get; set; }

        protected CaseHandler Result;
        protected CaseHandlerLoginModel handlerLogin = new CaseHandlerLoginModel();
        protected bool errorFlag;
        protected string errorMessage;
        //HttpContext HttpContext;


        protected override async Task OnInitializedAsync()
        {
            errorFlag = false;
            errorMessage = "";

            /// Fill the input field of username, if a previous username is saved in local storage
            try
            {
                var temp = await localStorage.GetItemAsync<CaseHandler>("caseHandlerObject");
                if (temp != null)
                {
                    handlerLogin.Username = temp.Username;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Localstorage fetching caseHandler for login: " + e.Message);
            }
        }

        /// <summary>
        ///  Clears input fields and error status
        /// </summary>
        protected void ClearClick()
        {
            ClearError();
            handlerLogin.Username = null;
            handlerLogin.Password = null;
        }

        /// <summary>
        /// If a valid submit type is entered, fetch login data from the DB based on username, and see if the password matches if a user is found
        /// </summary>
        /// <returns></returns>
        protected async Task HandleValidSubmit()
        {
            Console.WriteLine("OnValidSubmit");


            using (var client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage getApplication = await client.GetAsync("https://localhost:44399/api/casehandler/" + handlerLogin.Username);
                    if ((int)getApplication.StatusCode == 404)
                    {
                        Console.WriteLine(getApplication.StatusCode + ": Could not find this case handler");
                        errorFlag = true;
                        errorMessage = "Feil brukernavn og/ eller passord";
                        return;
                    }
                    getApplication.EnsureSuccessStatusCode();

                    var response = await getApplication.Content.ReadAsStringAsync();
                    Result = JsonConvert.DeserializeObject<CaseHandler>(response, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                    if ((int)getApplication.StatusCode == 200)
                    {
                        #region Hash input password
                        byte[] salt = Convert.FromBase64String(Result.Salt);

                        var pbkdf2 = new Rfc2898DeriveBytes(handlerLogin.Password, salt, 10000);

                        // Place hashed string in a byte array
                        byte[] hash = pbkdf2.GetBytes(20);

                        // Make new 36 long byte array to store hashed password (20 bytes) + salt (16 bytes)
                        byte[] hashBytes = new byte[36];

                        // place the hash and salt in their respective places
                        Array.Copy(salt, 0, hashBytes, 0, 16);
                        Array.Copy(hash, 0, hashBytes, 16, 20);

                        // Convert combined salt + hash to a string and store this inside hashedPassword
                        string hashedPassword = Convert.ToBase64String(hashBytes);
                        #endregion

                        if (Result.Password == hashedPassword)
                        {
                            #region non-functional authentication
                            //var identity = new ClaimsIdentity(new[] {
                            //    new Claim(ClaimTypes.Name, handlerLogin.Username),
                            //}, "Fake authentication type");
                            //_ = identity.IsAuthenticated;// = true;
                            //var user = new ClaimsPrincipal(identity);
                            //new AuthenticationState(user);

                            //var authState = await authenticationStateTask;
                            //authState.User.AddIdentity(identity);

                            //try
                            //{
                            //    HttpContext = new DefaultHttpContext
                            //    {
                            //        User = new ClaimsPrincipal(identity)
                            //    };
                            //    await HttpContext.SignInAsync(user);
                            //    HttpContext.User = user;

                            //}
                            //catch (Exception e)
                            //{

                            //    Console.WriteLine(e.Message);
                            //}
                            #endregion

                            ClearError();
                            await localStorage.SetItemAsync("caseHandlerObject", Result);
                            NavigationManager.NavigateTo("/cases");
                        }
                        else
                        {
                            errorMessage = "Feil brukernavn og/ eller passord";
                            errorFlag = true;
                        }
                    }
                    else
                    {
                        errorMessage = "Feil brukernavn og/ eller passord";
                        errorFlag = true;
                    }
                }

                catch (Exception e)
                {
                    errorFlag = true;
                    Console.WriteLine(e.Message);
                    throw;
                }
            }
        }

        /// <summary>
        /// Handle invalid submit
        /// </summary>
        protected void HandleInvalidSubmit()
        {
            Console.WriteLine("OnInvalidSubmit");
        }

        /// <summary>
        /// Debug functionality to check if there is an authenticated user or not
        /// </summary>
        /// <returns></returns>
        //protected async Task LogUsername()
        //{
        //    var authState = await authenticationStateTask;// AuthenticationStateProvider.GetAuthenticationStateAsync();
        //    var user = authState.User;

        //    if (user.Identity.IsAuthenticated)
        //    {
        //        Console.WriteLine($"{user.Identity.Name} is authenticated.");
        //    }
        //    else
        //    {
        //        Console.WriteLine("The user is NOT authenticated.");
        //    }
        //}

        /// <summary>
        /// Clears error mesage and status
        /// </summary>
        protected void ClearError()
        {
            errorFlag = false;
            errorMessage = "";
        }
    }

}
