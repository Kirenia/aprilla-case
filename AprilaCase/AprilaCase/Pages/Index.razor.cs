﻿using Aprila.Models;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AprilaCase.Pages
{
    public class IndexBase : ComponentBase
    {
        [Inject]
        NavigationManager NavigationManager { get; set; }

        public async Task BtnLogin()
        {
            NavigationManager.NavigateTo("/caseHandlerLogin");

        }

        public async Task BtnNewApli()
        {
            NavigationManager.NavigateTo("/newCustomer");

        }

        public async Task BtnReturning()
        {
            NavigationManager.NavigateTo("/customerLogin");

        }

    }
}
