﻿using Aprila.Models;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace AprilaCase.Pages
{
    public class CaseHandlerViewBase : ComponentBase
    {
        [Inject]
        NavigationManager NavigationManager { get; set; }
        [Inject]
        ILocalStorageService LocalStorage { get; set; }
        protected static List<Application> aplis = new List<Application>();
        protected List<Company> compys = new List<Company>();
        static readonly HttpClient client = new HttpClient();
        bool sorted = false;

        //Search textboxes variables
        public string nameSearch { get; set; } = "";
        public string orgNrSearch { get; set; } = "";
        public string sakNrSearch { get; set; } = "";

        protected CaseHandler caseHandler = new CaseHandler();

        protected override async Task OnInitializedAsync()
        {
            //trying to get a casehandler object from local storage
            try
            {
                caseHandler = await LocalStorage.GetItemAsync<CaseHandler>("caseHandlerObject");
            }
            catch (Exception e)
            {
            }
            finally
            {
                //this is to make sure a casehandler object is in the local stoarge, i.e the user completed the login process.
                if (caseHandler == null)
                {
                    NavigationManager.NavigateTo("/caseHandlerLogin");
                }
            }
            //empties array of applications.
            aplis.Clear();
            //GETs all companies and stores them in a list.
            HttpResponseMessage getComp = await client.GetAsync("https://localhost:44399/api/companies");
            getComp.EnsureSuccessStatusCode();
            var responseBodyComp = await getComp.Content.ReadAsStringAsync();
            compys = JsonConvert.DeserializeObject<List<Company>>(responseBodyComp, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //loop through list of companies.
            foreach (var comp in compys)
            {
                //GETs all applications and adds them to the companies applcation list.
                HttpResponseMessage getAppli = await client.GetAsync("https://localhost:44399/api/companies/" + comp.OrgNumber + "/applications");
                getAppli.EnsureSuccessStatusCode();
                var responseBodyAplli = await getAppli.Content.ReadAsStringAsync();
                comp.Applications = JsonConvert.DeserializeObject<List<Application>>(responseBodyAplli, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                //Loops through all the applications for the company and adds it to the aplis list.
                foreach (var apli in comp.Applications)
                {
                    aplis.Add(new Application { CaseNumber = apli.CaseNumber, Accepted = apli.Accepted, Active = apli.Active, DateCreated = apli.DateCreated, Comment = apli.Comment, Company = new Company() { OrgName = comp.OrgName, OrgNumber = comp.OrgNumber } });
                }
            }
            //defaults sorts by date.
            aplis = SortByDateAsc(aplis);
        }
        //Navigate to the selected task.
        public async Task GoToCase(int numb)
        {
            var url = "/case/" + numb;
            NavigationManager.NavigateTo(url);
        }
        //4 sorting functions.
        public static List<Application> SortByDateAsc(List<Application> list)
        {
            list.Sort((a, b) => a.DateCreated.CompareTo(b.DateCreated));
            return list;
        }
        public static List<Application> SortByDateDesc(List<Application> list)
        {
            list.Sort((a, b) => b.DateCreated.CompareTo(a.DateCreated));
            return list;
        }

        public static List<Application> SortByNameDesc(List<Application> list)
        {
            list.Sort((a, b) => b.Company.OrgName.CompareTo(a.Company.OrgName));
            return list;
        }
        public static List<Application> SortByNameAsc(List<Application> list)
        {
            list.Sort((a, b) => a.Company.OrgName.CompareTo(b.Company.OrgName));
            return list;
        }
        //Onclick for the name sort button.
        public void SortByNameBtn()
        {
            List<Application> tempList = aplis.ToList();
            aplis.Clear();
            if (sorted)
            {
                aplis = SortByNameAsc(tempList);
                sorted = !sorted;
            }
            else
            {
                aplis = SortByNameDesc(tempList);
                sorted = !sorted;
            }
        }
        //Onclick for the date sort button.
        public void SortByDateBtn()
        {
            List<Application> tempList = aplis.ToList();
            aplis.Clear();
            if (sorted)
            {
                aplis = SortByDateAsc(tempList);
                sorted = !sorted;
            }
            else
            {
                aplis = SortByDateDesc(tempList);
                sorted = !sorted;

            }

        }
        //Functions to clear the other search boxes while one is being used. 
        //To prevent search with multiple parameteres
        public void clearOrgName()
        {
            sakNrSearch = "";
            orgNrSearch = "";
        }
        public void clearOrgNr()
        {
            sakNrSearch = "";
            nameSearch = "";
        }
        public void clearCaseNr()
        {
            orgNrSearch = "";
            nameSearch = "";
        }
    }
}

