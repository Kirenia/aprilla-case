﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;



namespace AprilaCase.Pages
{
    
    public class ConfirmApproveBase : ComponentBase
    {
        

        [Parameter]
        public bool Deny { get; set; }
        [Parameter]
        public int Case { get; set; }
        [Parameter]
        public int OrgNr { get; set; }
        [Inject]
        NavigationManager NavigationManager { get; set; }


        public async Task GoBack()
        {
            NavigationManager.NavigateTo("/case/" + Case.ToString());


        }

        public async Task PostStatus()
        {
            var urlAccept = "https://localhost:44399/api/companies/" + OrgNr + "/applications/" + Case + "/accept";
            var urlDeny = "https://localhost:44399/api/companies/" + OrgNr + "/applications/" + Case + "/inactive";
            HttpClient client = new HttpClient();
            if (Deny)
            {
                //PUT
                //Set Active false and Accepted false

                var json = AppliToJson(false, false);
                var response = await client.PutAsync(urlDeny, new StringContent("", Encoding.UTF8, "application/json")).ConfigureAwait(false);
                var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                NavigationManager.NavigateTo("/cases");

            }
            else
            {
                //PUT
                //Set Active false and Accepted true
                var json = AppliToJson(false, true);
                var response = await client.PutAsync(urlAccept, new StringContent("", Encoding.UTF8, "application/json")).ConfigureAwait(false);
                var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                NavigationManager.NavigateTo("/cases");
            }

        }

        public static string AppliToJson(bool active, bool Accepted)
        {
            string json = "{\"accepted\":" + Accepted.ToString().ToLower() + ",\"active\":" + active.ToString().ToLower() + "}";

            return json;
        }

    }
}
