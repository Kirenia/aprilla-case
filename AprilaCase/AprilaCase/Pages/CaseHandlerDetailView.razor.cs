﻿using Aprila.Models;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace AprilaCase.Pages
{
    public class CaseHandlerDetailViewBase : ComponentBase
    {
        [Inject]
        NavigationManager NavigationManager { get; set; }
        [Inject]
        ILocalStorageService LocalStorage { get; set; }
        //Bind variables
        public string CusComment { get; set; } = "";
        public string IntComment { get; set; } = "";
        public string Result { get; set; } = "";
        public bool PepChecked { get; set; } = false;
        public string Summary { get; set; } = "";

        static readonly HttpClient client = new HttpClient();
        [Parameter]

        public int CaseNumber { get; set; }

        protected ApplicationRM appRM = new ApplicationRM();
        protected Company company = new Company();
        protected KYCRM kyc = new KYCRM();
        protected List<ApplicationRM> oldAppli = new List<ApplicationRM>();
        protected CaseHandler caseHandler = new CaseHandler();
        dynamic OrgData;

        protected override async Task OnInitializedAsync()
        {
            //trying to get a casehandler object from local storage
            try
            {
                caseHandler = await LocalStorage.GetItemAsync<CaseHandler>("caseHandlerObject");
            }
            catch (Exception e)
            {
            }
            finally
            {
                //this is to make sure a casehandler object is in the local stoarge, i.e the user completed the login process.
                if (caseHandler == null)
                {
                    NavigationManager.NavigateTo("/caseHandlerLogin");
                }
            }
            //Getting the current application based on the Casenumber parameter
            HttpResponseMessage getAppli = await client.GetAsync("https://localhost:44399/api/applications/" + CaseNumber);
            getAppli.EnsureSuccessStatusCode();
            var responseBodyAplli = await getAppli.Content.ReadAsStringAsync();
            appRM = JsonConvert.DeserializeObject<ApplicationRM>(responseBodyAplli, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            //Getting company for this casenumber
            HttpResponseMessage getComp = await client.GetAsync("https://localhost:44399/api/companies/" + appRM.CompanyOrgNumber);
            getComp.EnsureSuccessStatusCode();
            var responseBodyComp = await getComp.Content.ReadAsStringAsync();
            company = JsonConvert.DeserializeObject<Company>(responseBodyComp, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

            //Getting KYC questions for this company's apllication
            HttpResponseMessage getKYC = await client.GetAsync("https://localhost:44399/api/companies/" + appRM.CompanyOrgNumber + "/applications/" + appRM.CaseNumber + "/kyc");
            var responseBodyKYS = await getKYC.Content.ReadAsStringAsync();
            getComp.EnsureSuccessStatusCode();
            kyc = JsonConvert.DeserializeObject<KYCRM>(responseBodyKYS, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            
            //Setting the text of the internal comment textarea to casehandler's name and date
            IntComment = caseHandler.Firstname + " " + caseHandler.Lastname + ": " + DateTime.Now + "\n\n\n";
            string internalNote = "";
            //replacing symbol used to make the strings single line
            if (appRM.InternalNote != null)
            {
                internalNote = appRM.InternalNote.Replace("·", "\n");

            }
            if (appRM.Comment != null)
            {
                appRM.Comment = appRM.Comment.Replace("·", "\n");
            }
            //get the other cases this company has had
            await OldCases();
            //Add old internal notes to textarea
            IntComment += internalNote;
            GetSummary();
        }
        //Getting data from Brreg's API.
        public async Task CheckOrgNum()
        {
            Result = "Fant ikke noe ifra Brreg";
            var JsonString = "";
            var url = "https://data.brreg.no/enhetsregisteret/api/enheter/" + company.OrgNumber;
            HttpClient http = new HttpClient();
            JsonString = await http.GetStringAsync(url);
            OrgData = JsonConvert.DeserializeObject<dynamic>(JsonString);
            if (JsonString != "")
            {
                Result = OrgData.navn + "\n";
                Result += OrgData.naeringskode1.beskrivelse + "\n";
                Result += ((bool)OrgData.konkurs) ? "Konkurs \n"  : "Ikke konkurs \n";
            }
            else
            {
                Result = "Fant ikke noe ifra Brreg";
            }
            GetSummary();


        }
        //Accept button
        public async Task BtnAccept()
        {
            NavigationManager.NavigateTo("/confirm/false/" + CaseNumber.ToString() + "/" + company.OrgNumber);

        }
        //Deny button
        public async Task BtnDeny()
        {
            NavigationManager.NavigateTo("/confirm/true/" + CaseNumber.ToString() + "/" + company.OrgNumber);

        }
        //go to a previous case
        public async Task OtherCase(int oldCase)
        {
            NavigationManager.NavigateTo("/case/" + oldCase.ToString(), true);
        }
        //Return to case overview
        public async Task BtnGoBack()
        {
            NavigationManager.NavigateTo("/cases");
        }

        //send mail to customer
        public async Task CustomerComment()
        {
            try
            {
                //Message 
                //string melding = "Hei<br>"; 
                //// UNCOMMENT:
                ////melding += "For å se oppdateringene i saken deres vennligst gå inn på aprila.no og logg inn med organisasjonsnummeret og saksnummer: " + CaseNumber;
                ////melding += "<br>Mvh<br>Aprila<br>saksbehandler";
                //melding += "Dette er en testmail. se bortifra denne";
                //MailMessage message = new MailMessage();
                //SmtpClient smtp = new SmtpClient();
                //message.From = new MailAddress(email);
                //message.To.Add(new MailAddress(kyc.Email)); 
                //message.Subject = "Oppdatering i søkenden";
                //message.IsBodyHtml = true; //to make message body as html
                //message.Body = melding;
                ////SMTP 
                //smtp.UseDefaultCredentials = false;
                //smtp.Port = 587;
                //smtp.Host = "smtp.gmail.com"; //for gmail host
                //smtp.EnableSsl = true;
                //smtp.Credentials = new NetworkCredential(email, password); // probably shouldn't be hardcoded
                //smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                //smtp.Send(message);
                //PUT comment to database
                var url = "https://localhost:44399/api/companies/" + company.OrgNumber + "/applications/" + CaseNumber + "/comment";
                string parseString = (appRM.Comment + "·" + DateTime.Now.ToString() + ": " + CusComment).Replace("\r\n", "·").Replace("\n", "·").Replace("\r", "·");
                Application app = new Application();
                app.Comment = parseString;
                string jsonObject = JsonConvert.SerializeObject(app);
                var response = await client.PutAsync(url, new StringContent(jsonObject, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                //appRM.Comment += "\n" + caseHandler.Firstname + " " + caseHandler.Lastname + ": " + DateTime.Now.ToString() + ": " + CusComment;
                appRM.Comment += "\n" + DateTime.Now.ToString() + ": " + CusComment;
                CusComment = "";
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                CusComment = e.ToString();
            }
        }
        //Save button for internal note, PUTs info to database
        public async Task InternalComment()
        {
            var url = "https://localhost:44399/api/companies/" + company.OrgNumber + "/applications/" + CaseNumber + "/internalnote";
            string parseString = IntComment.Replace("\r\n", "·").Replace("\n", "·").Replace("\r", "·");
            Application app = new Application();
            app.InternalNote = parseString;
            string temp = JsonConvert.SerializeObject(app);
            var response = await client.PutAsync(url, new StringContent(temp, Encoding.UTF8, "application/json")).ConfigureAwait(false);
            var responseString = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            appRM.InternalNote = IntComment;
            IntComment = caseHandler.Firstname + " " + caseHandler.Lastname + ": " + DateTime.Now + "\n\n\n";
            IntComment += appRM.InternalNote;
        }
        //GETs all the previous applications and stores them in a list.
        public async Task OldCases()
        {
            HttpResponseMessage getAppli = await client.GetAsync("https://localhost:44399/api/companies/" + company.OrgNumber + "/applications");
            getAppli.EnsureSuccessStatusCode();
            var responseBodyAplli = await getAppli.Content.ReadAsStringAsync();
            oldAppli = JsonConvert.DeserializeObject<List<ApplicationRM>>(responseBodyAplli, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

        }
        public void GetSummary()
        {
            int Errors = 0;
            Summary = "";

            if (kyc.PEP)
            {
                Summary += "⚠ PEP! \n";
                Errors++;
            }
            if (!kyc.SignatureRight)
            {
                Summary += "⚠ Ingen signaturrett \n";
                Errors++;
            }
            if (!kyc.BelongNorway)
            {
                Summary += "⚠ Skatter ikke til Norge \n";
                Errors++;
            }
            if (kyc.MonthlyTurnover < 100000)
            {
                Summary += "⚠ Lav omsettning \n";
                Errors++;                
            }
            if( OrgData != null && (bool)OrgData.konkurs)
            {
                Summary += "⚠ Konkurs \n";
                Errors++;
            }
            if ( Errors == 0)
            {
                Summary = "✔ Alt ok";
            }
        }
    }    
}
