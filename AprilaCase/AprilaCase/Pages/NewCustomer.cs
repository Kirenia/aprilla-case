﻿using Aprila.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text;
using System.Linq;
using System.Diagnostics;



namespace AprilaCase.Pages
{
    public class NewCustomerBase : ComponentBase
    {

        public int caseNumber1 { get; set; }

        public Company company = new Company();

        public KYCRM kyc = new KYCRM();
        KYC kycObject;

        public int? organisasjonsnummer { get; set; }

        [Inject]
        ILogger<ApplicationRM> Logger { get; set; }
        [Inject]
        NavigationManager navigationManager { get; set; }
        public bool loadFailed { get; set; }
        public string Land { get; set; }
        public DateTime date { get; set; }

        public string _selectedCountyName;
        public string SelectedCountryName
        {
            get => _selectedCountyName;
            set
            {
                _selectedCountyName = value;
            }
        }
        public CountryNorsk Lands;


        // Check if the company is registered in the enhetsregisteret API.
        public async Task CheckDataApiER()
        {
            using (var client = new HttpClient())
            {
                if (organisasjonsnummer != null)
                {
                    loadFailed = false;
                    try
                    {
                        HttpResponseMessage getInfo = await client.GetAsync("https://data.brreg.no/enhetsregisteret/api/enheter/" + organisasjonsnummer);
                        getInfo.EnsureSuccessStatusCode();
                        var response = await getInfo.Content.ReadAsStringAsync();
                        CompanyER companyER = JsonConvert.DeserializeObject<CompanyER>(response, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                        company.OrgName = companyER.navn;
                        company.OrgNumber = Int32.Parse(companyER.organisasjonsnummer);
                        company.Bankrupt = companyER.konkurs;
                        if (companyER.postadresse != null)
                        {
                            kyc.Country = companyER.postadresse.land;
                        }
                        else
                        {
                            kyc.Country = companyER.forretningsadresse.land;
                        }
                        await postCompany(company);
                    }
                    catch (Exception ex)
                    {
                        loadFailed = true;

                        Logger.LogWarning(ex, "Failed to load  {Company number}", organisasjonsnummer);
                    }
                }
            }
        }

        //Post the company before the application is submited 
        public async Task postCompany(Company addCompany)
        {
            using (HttpClient client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage getInfo = await client.GetAsync("https://localhost:44399/api/companies/" + addCompany.OrgNumber);
                    if (!getInfo.IsSuccessStatusCode)
                    {
                        string jsonCompany = JsonConvert.SerializeObject(addCompany);
                        Uri url = new Uri("https://localhost:44399/api/companies");

                        var response = await client.PostAsync(url, new StringContent(jsonCompany, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                        var result = await response.Content.ReadAsStringAsync();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex);
                }
            }
        }

        //Submit the application form 
        public async Task SubmitData()
        {
            Logger.LogDebug("HEI.");
            kycObject = new KYC();

            kycObject.AccountNumber = kyc.AccountNumber;
            kycObject.DateCreated = DateTime.Now;
            kycObject.Email = kyc.Email;
            kycObject.Company = company;
            kycObject.ContactPerson = kyc.ContactPerson;
            kycObject.Phone = kyc.Phone;
            kycObject.SignatureRight = kyc.SignatureRight;
            kycObject.Country = kyc.Country;
            kycObject.Purpose = kyc.Purpose;
            kycObject.MonthlyTurnover = kyc.MonthlyTurnover;
            kycObject.SelectedCountry = SelectedCountryName;
            kycObject.ContactPerson = kyc.ContactPerson;


            using (HttpClient client = new HttpClient())
            {
                try
                {
                    Console.WriteLine(kycObject.Company.OrgNumber);
                    string json = JsonConvert.SerializeObject(kycObject);
                    //Uri url = new Uri("https://localhost:44399/api/companies/" + organisasjonsnummer + "/kyc");
                    Uri url = new Uri("https://localhost:44399/api/companies/" + kycObject.Company.OrgNumber + "/kyc");

                    var response = await client.PostAsync(url, new StringContent(json, Encoding.UTF8, "application/json")).ConfigureAwait(false);
                    var result = await response.Content.ReadAsStringAsync();

                    //This code will find the casenumber and print it out.
                    //The applications need to use this for logging in to the status side.  
                    KYC newKYC = JsonConvert.DeserializeObject<KYC>(result);

                    Debug.WriteLine("-----------------------------------------");
                    Debug.WriteLine(newKYC.Applications.First().CaseNumber);
                    Debug.WriteLine("-----------------------------------------");

                    caseNumber1 = newKYC.Applications.First().CaseNumber;

                    navigationManager.NavigateTo($"/component/{caseNumber1}");
                }
                catch (Exception ex)
                {
                    //Message to the user
                    Logger.LogWarning(ex, "Failed to post applivation {Company number}", organisasjonsnummer);
                }
            }
        }

        //Call the Get country method when the program is Initialized
        protected override Task OnInitializedAsync()
        {
            return GetCountries1();
        }


        //Get the List of Countries from the a country Api
        public async Task GetCountries1()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    HttpResponseMessage getInfo = await client.GetAsync("http://data.ssb.no/api/klass/v1/versions/377");
                    var responseBodycountries = await getInfo.Content.ReadAsStringAsync();
                    Lands = JsonConvert.DeserializeObject<CountryNorsk>(responseBodycountries, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });

                    string alpha = "ABCDEFGHIJKLMNOPQRSTUVQXYZ";

                    foreach (char c in alpha)
                    {
                        //Order the List of countries
                        Lands.classificationItems = Lands.classificationItems.OrderBy(country => country.name).ToArray();
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogWarning(ex, "Failed to load  {Company number}", organisasjonsnummer);

                }
            }
        }
    }
}


