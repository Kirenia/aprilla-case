﻿using System.ComponentModel.DataAnnotations;

namespace AprilaCase.Data
{
    public class CustomerLoginModel
    {
        [Required(ErrorMessage = "An organization number is required in order to view an application")]
        [RegularExpression(@"^[0-9]{9}$", ErrorMessage = "Please enter a valid organization number (9 digits)")]
        [Display(Name = "Organization number")]
        public int? orgNo { get; set; }

        [Required(ErrorMessage = "A case number is required in order to view an application")]
        public int? caseNo { get; set; }

        public string Token { get; set; }
    }
}

