﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AprilaCase.Models
{
    public class CaseHandlerLoginModel
    {

        [Required(ErrorMessage = "Et brukernavn må oppgis for å logge inn")]
        [Display(Name = "Brukernavn")]
        [RegularExpression(@"^[^{}<>;:'*]+$", ErrorMessage = "Visse spesialtegn er ikke tillat. ")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Et passord må oppgis for å logge inn")]
        [RegularExpression(@"^[^{}<>;:'*]+$", ErrorMessage = "Visse spesialtegn er ikke tillat.  ")]
        public string Password { get; set; }

        public string Token { get; set; }
    }
}
