using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

using Aprila.Models;
using Microsoft.EntityFrameworkCore;
using Aprila.CQRS.Queries;
using System.Reflection;
using Aprila.CQRS.Commands;

namespace Aprila.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Tells the program which files import which Interface
            services.AddScoped<IQueriesService, QueriesService>();
            services.AddScoped<ICommandService, CommandService>();

            // Gets the connectionstring from the appsettings.json
            services.AddDbContext<DataContext>(optionsAction: optionsBuilder =>
               optionsBuilder.UseSqlServer(Configuration["ConnectionString"],
               optionsAction => optionsAction.MigrationsAssembly(typeof(DataContext).GetTypeInfo().Assembly.GetName().Name)));

            services.AddMvc(option => option.EnableEndpointRouting = false)
                .SetCompatibilityVersion(Microsoft.AspNetCore.Mvc.CompatibilityVersion.Version_3_0)
                .AddNewtonsoftJson(opt => opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
