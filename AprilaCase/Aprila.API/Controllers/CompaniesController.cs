﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aprila.CQRS.Commands;
using Aprila.CQRS.Queries;
using Aprila.Models;
using Microsoft.AspNetCore.Mvc;

namespace Aprila.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompaniesController : ControllerBase
    {
        private readonly IQueriesService _queries;
        private readonly ICommandService _commands;

        public CompaniesController(IQueriesService queries, ICommandService commands)
        {
            _queries = queries;
            _commands = commands;
        }

        // ------------------------------------------- COMPANIES ------------------------------------------
        // GET api/companies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Company>>> Get()
        {
            return (await _queries.GetAllCompanies()).ToList();
        }
        
        // GET api/companies/{orgnumber}
        [HttpGet("{orgNumber}")]
        public async Task<ActionResult<Company>> GetCompany(int orgNumber)
        {
            if (!await _queries.CompanyExists(orgNumber))
            {
                return NotFound();
            }
            return await _queries.GetCompanyByOrgNumber(orgNumber);
        }
        
        // POST api/companies
        [HttpPost]
        public async Task<IActionResult> AddCompany([FromBody] Company company)
        {
            await _commands.CreateCompany(company);
            return NoContent();
        }

        // PUT api/companies/{orgNumber}
        [HttpPut("{orgNumber}")]
        public async Task<IActionResult> UpdateCompany([FromRoute] int orgNumber, [FromBody] Company company)
        {
            await _commands.UpdateCompany(orgNumber, company);
            return NoContent();
        }

        // PUT api/companies/{orgNumber}/bankrupt
        [HttpPut("{orgNumber}/bankrupt")]
        public async Task<IActionResult> CompanyBankrupt([FromRoute] int orgNumber)
        {
            await _commands.CompanyBankrupt(orgNumber);
            return NoContent();
        }

        // ------------------------------------------------------------------------------------------------


        // --------------------------------------------- APPLICAIONS ---------------------------------------
        // GET api/companies/{orgnumber}/applications
        [HttpGet("{orgNumber}/applications")]
        public async Task<ActionResult<IEnumerable<ApplicationRM>>> GetCompanyApplications(int orgNumber)
        {
            return (await _queries.GetAllCompanyApplications(orgNumber)).ToList();
        }

        // GET api/companies/{orgnumber}/applications/{caseNumber}
        [HttpGet("{orgNumber}/applications/{caseNumber}")]
        public async Task<ActionResult<ApplicationRM>> GetCompanyApplication(int orgNumber, int caseNumber)
        {

            if(!await _queries.CompanyApplicationExists(orgNumber, caseNumber))
            {
                return NotFound();
            }
            return await _queries.GetCompanyApplication(orgNumber, caseNumber);
        }

        // POST api/companies/{orgNumber}/applications
        // CREATE APPLICATION
        [HttpPost("{orgNumber}/applications")]
        public async Task<IActionResult> AddCompanyApplication([FromRoute] int orgNumber)
        {
            await _commands.CreateApplication(orgNumber);
            return NoContent();
        }

        // PUT api/companies/{orgNumber}/applications/{caseNumber}/comment
        // SET APPLICATION_COMMENT TO STRING FROM BODY
        [HttpPut("{orgNumber}/applications/{caseNumber}/comment")]
        public async Task<IActionResult> UpdateApplicationComment([FromRoute] int caseNumber, [FromBody] Application application)
        {
            await _commands.UpdateApplicationComment(caseNumber, application.Comment);
            return NoContent();
        }

        // PUT api/companies/{orgNumber}/applications/{caseNumber}/internalnote
        // SET APPLICATION_INTERNALNOTE TO STRING FROM BODY
        [HttpPut("{orgNumber}/applications/{caseNumber}/internalNote")]
        public async Task<IActionResult> UpdateApplicationInternalNote([FromRoute] int caseNumber, [FromBody] Application application)
        {
            await _commands.UpdateApplicationInternalNote(caseNumber, application.InternalNote);
            return NoContent();
        }

        // PUT api/companies/{orgNumber}/applications/{caseNumber}/accept
        // SET APPLICATION TO ACCEPTED = TRUE
        [HttpPut("{orgNumber}/applications/{caseNumber}/accept")]
        public async Task<IActionResult> AcceptApplication([FromRoute] int caseNumber)
        {
            await _commands.AcceptApplication(caseNumber);
            return NoContent();
        }

        // PUT api/companies/{orgNumber}/applications/{caseNumber}/inactive
        // SET APPLICATION TO ACTIVE = FALSE
        [HttpPut("{orNumber}/applications/{caseNumber}/inactive")]
        public async Task<IActionResult> InactiveApplication([FromRoute] int caseNumber)
        {
            await _commands.InactiveApplication(caseNumber);
            return NoContent();
        }

        // DELETE api/companies/{orgNumber}/applications/{caseNumber}
        // SET APPLICATION TO DELETED = TRUE
        [HttpDelete("{orgNumber}/applications/{caseNumber}")]
        public async Task<IActionResult> DeleteCompanyApplication([FromRoute] int orgNumber, [FromRoute] int caseNumber)
        {
            await _commands.SetApplicationDeleted(caseNumber);
            return NoContent();
        }
        // ------------------------------------------------------------------------------------------------


        // --------------------------------------------- KYCs ----------------------------------------------
        // GET api/companies/{orgNumber}/kyc
        [HttpGet("{orgNumber}/kyc")]
        public async Task<IEnumerable<KYCRM>> GetKYCs([FromRoute] int orgNumber)
        {
            return (await _queries.GetKYCs(orgNumber)).ToList();
        }

        // GET api/companies/{orgNumber}/applications/{caseNumber}/kyc
        [HttpGet("{orgNumber}/applications/{caseNumber}/kyc")]
        public async Task<ActionResult<KYCRM>> GetApplicationKYC([FromRoute] int caseNumber)
        {
            if (!await _queries.ApplicationExistst(caseNumber))
            {
                return NotFound();
            }
            return await _queries.GetKYC(caseNumber);
        }
        // ------------------------------------------------------------------------------------------------
    }
}