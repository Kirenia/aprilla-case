﻿using System.Threading.Tasks;
using Aprila.CQRS.Commands;
using Aprila.CQRS.Queries;
using Aprila.Models;
using Microsoft.AspNetCore.Mvc;

namespace Aprila.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CaseHandlerController : ControllerBase
    {
        private readonly IQueriesService _queries;
        private readonly ICommandService _commands;

        public CaseHandlerController(IQueriesService queries, ICommandService commands)
        {
            _queries = queries;
            _commands = commands;
        }

        // GET api/casehandler/{username}
        [HttpGet("{username}")]
        public async Task<ActionResult<CaseHandler>> GetCaseHandler([FromRoute] string username)
        {
            if (!await _queries.UsernameExists(username))
            {
                return NotFound();
            }

            return await _queries.GetCaseHandlerByUsername(username);
        }

        // POST api/casehandler
        [HttpPost]
        public async Task<ActionResult> PostCaseHandler([FromBody] CaseHandler caseHandler)
        {
            await _commands.CreateCaseHandler(caseHandler);
            return NoContent();
        }
    }
}