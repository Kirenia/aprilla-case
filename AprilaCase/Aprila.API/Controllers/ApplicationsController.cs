﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Aprila.CQRS.Commands;
using Aprila.CQRS.Queries;
using Aprila.Models;
using Microsoft.AspNetCore.Mvc;

namespace Aprila.API.Controllers
{
    [Route("api/applications")]
    [ApiController]
    public class ApplicationsController : ControllerBase
    {
        private readonly IQueriesService _queries;
        private readonly ICommandService _commands;
        public ApplicationsController(IQueriesService queries, ICommandService commands)
        {
            _queries = queries;
            _commands = commands;
        }

        // GET api/applications
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ApplicationRM>>> Get()
        {
            return (await _queries.GetAllApplications()).ToList();
        }

        // GET api/applications/{casenumber}
        [HttpGet("{caseNumber}")]
        public async Task<ActionResult<ApplicationRM>> GetApplication(int caseNumber)
        {
            if (!await _queries.ApplicationExistst(caseNumber))
            {
                return NotFound();
            }
            return await _queries.GetApplicationByCaseNumber(caseNumber);
        }

        // PUT api/applications/{casenumber}/casehandler
        [HttpPut("{caseNumber}/casehandler")]
        public async Task<ActionResult> SetApplicationCaseHandler([FromRoute] int caseNumber, [FromBody] CaseHandler caseHandler)
        {
            await _commands.SetApplicationCaseHandler(caseNumber, caseHandler.Username);
            return NoContent();
        }
    }
}