﻿using System.Threading.Tasks;
using Aprila.CQRS.Commands;
using Aprila.CQRS.Queries;
using Aprila.Models;
using Microsoft.AspNetCore.Mvc;

namespace Aprila.API.Controllers
{
    //[Route("api/[controller]")]
    [Route("api/companies")]
    [ApiController]
    public class KYCController : ControllerBase
    {
        private readonly IQueriesService _queries;
        private readonly ICommandService _commands;

        public KYCController(IQueriesService queries, ICommandService commands)
        {
            _queries = queries;
            _commands = commands;
        }

        // POST api/companies/{orgNumber}/kyc
        [HttpPost("{orgNumber}/kyc")]
        public async Task<ActionResult<KYC>> PostKYC([FromBody] KYC kyc, [FromRoute] int orgNumber)
        {
            return await _commands.AddKYC(kyc, orgNumber);
        }
    }
}