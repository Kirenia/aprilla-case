﻿using Aprila.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Aprila.CQRS.Commands
{
    public interface ICommandService
    {
        // Company Commands
        Task<Company> CreateCompany(Company company);
        Task UpdateCompany(int orgNumber, Company company);
        Task CompanyBankrupt(int orgNumber);

        // Application Commands
        Task<Application> CreateApplication(int orgNumber);
        Task UpdateApplicationComment(int caseNumber, string comment);
        Task UpdateApplicationInternalNote(int caseNumber, string internalNote);
        Task SetApplicationDeleted(int caseNumber);
        Task AcceptApplication(int caseNumber);
        Task InactiveApplication(int caseNumber);
        Task SetApplicationCaseHandler(int casenumber, string username);

        // CaseHandler Commands
        Task<CaseHandler> CreateCaseHandler(CaseHandler casehandler);

        // KYC Commands
        Task<KYC> AddKYC(KYC kyc, int orgNumber);

    }
}
