﻿using Aprila.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Aprila.CQRS.Commands
{
    public class CommandService : ICommandService
    {
        public readonly DataContext _context;

        public CommandService(DataContext context)
        {
            _context = context;
        }

        // Company Commands
        public async Task<Company> CreateCompany(Company comp)
        {
            Company company;
            // Checks if a company with the given orgnumber alreadu exists
            if (await CompanyExists(comp.OrgNumber))
            {
                // If a company already exists, set the "company" to this company-object
                company = await _context.Companies.SingleAsync(c => c.OrgNumber == comp.OrgNumber);
                return company;
            }

            // Sets "company" to the company sent as a parameter
            company = comp;
            // Add to companies list in the context and save
            await  _context.Companies.AddAsync(company);
            await _context.SaveChangesAsync();

            return company;
        }
        public async Task UpdateCompany(int orgNumber, Company company)
        {
            // Sets the company orgnumber equal to the given orgnumber
            company.OrgNumber = orgNumber;
            // Updates the state of the company and saves
            _context.Entry(company).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task CompanyBankrupt(int orgNumber)
        {
            // Finds the company with the given orgnumber
            Company company = await _context.Companies.SingleAsync(c => c.OrgNumber == orgNumber);

            // Sets the company-bankrupt field to true
            company.Bankrupt = true;

            // Updates the state of the company and saves
            _context.Entry(company).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        // Use to check if company already exists
        private async Task<bool> CompanyExists(int orgNumber)
        {
            return await _context.Companies.AnyAsync(c => c.OrgNumber == orgNumber);
        }

        // Application Commands
        public async Task<Application> CreateApplication(int orgNumber)
        {
            // Create a new instance of an application
            Application application = new Application();
            try
            {
                // Sets the date, comment, internal note and active-tag
                application.DateCreated = DateTime.Now;
                application.Comment = "";
                application.InternalNote = "";
                application.Active = true;

                // Finds the company with the given orgnumber
                Company company = await _context.Companies.SingleAsync(c => c.OrgNumber == orgNumber);
                application.Company = company;

                _context.Applications.Add(application);

                // Adds the new application to the list of applications inside the company
                company.Applications.Add(application);

                // Updates the state and saves
                _context.Entry(company).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException){
                
            }
            
            return application;
        }
        public async Task UpdateApplicationComment(int caseNumber, string comment)
        {
            Application application = await _context.Applications.SingleAsync(a => a.CaseNumber == caseNumber);
            application.Comment = comment;
            _context.Entry(application).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task UpdateApplicationInternalNote(int caseNumber, string internalNote)
        {
            Application application = await _context.Applications.SingleAsync(a => a.CaseNumber == caseNumber);
            application.InternalNote = internalNote;
            _context.Entry(application).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task SetApplicationDeleted(int caseNumber)
        {
            // Sets the application as deleted
            // This application is no longer available from a GET
            Application application = await _context.Applications.SingleAsync(a => a.CaseNumber == caseNumber);
            application.Deleted = true;
            application.Active = false;
            _context.Entry(application).State = EntityState.Modified;
            await _context.SaveChangesAsync();
        }
        public async Task AcceptApplication(int caseNumber)
        {
            Application application = await _context.Applications.SingleAsync(a => a.CaseNumber == caseNumber);
            application.Accepted = true;

            _context.Entry(application).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                
            }
            await InactiveApplication(caseNumber);
        }
        public async Task InactiveApplication(int caseNumber)
        {
            Application application = await _context.Applications.SingleAsync(a => a.CaseNumber == caseNumber);
            application.Active = false;

            _context.Entry(application).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {

            }
        }
        // Used to set the handler of an application
        public async Task SetApplicationCaseHandler(int casenumber, string username)
        {
            var caseHandler = await _context.CaseHandlers.SingleAsync(c => c.Username == username);
            var application = await _context.Applications.SingleAsync(a => a.CaseNumber == casenumber);

            application.CaseHandler = caseHandler;
            _context.Entry(application).State = EntityState.Modified;
            try
            {
                await _context.SaveChangesAsync();
            }
            catch(DbUpdateConcurrencyException)
            {

            }
        }

        // Case Handler commands
        public async Task<CaseHandler> CreateCaseHandler(CaseHandler casehandler)
        {
            // Checks if an user with the given username already exists
            if (await UsernameExists(casehandler.Username))
            {
                throw new Exception("Username already exists");
            }

            CaseHandler caseHandler = new CaseHandler(casehandler.Firstname, casehandler.Lastname, casehandler.Username, casehandler.Password);

            await _context.CaseHandlers.AddAsync(caseHandler);
            await _context.SaveChangesAsync();

            return caseHandler;
        }
        // Internal methods
        private async Task<bool> UsernameExists(string username)
        {
             return await _context.CaseHandlers.AnyAsync(ch => ch.Username == username);
        }

        // KYC Commands
        public async Task<KYC> AddKYC(KYC kyc, int orgNumber)
        {
            KYC newKYC = kyc;
            try
            {
                Company company = await _context.Companies.SingleAsync(c => c.OrgNumber == orgNumber);

                // Creates a new application which the KYC is connected to
                Application application = new Application()
                {
                    DateCreated = DateTime.Now,
                    Comment = "",
                    InternalNote = "",
                    Active = true
                };
                application.Company = company;

                newKYC.Company = company;
                newKYC.DateCreated = DateTime.Now;
                newKYC.Applications.Add(application);

                company.KYCs.Add(newKYC);
                application.KYC = newKYC;

                _context.Entry(company).State = EntityState.Modified;

                await _context.KYCs.AddAsync(newKYC);
                await _context.Applications.AddAsync(application);
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateException)
            {
                
            }
            return newKYC;
        }
    }
}
