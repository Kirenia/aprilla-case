﻿using Aprila.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Newtonsoft.Json;
using Microsoft.EntityFrameworkCore;

namespace Aprila.CQRS.Queries
{
    public class QueriesService : IQueriesService
    {
        private readonly DataContext _context;
        public QueriesService(DataContext context)
        {
            _context = context;
        }

        // Company queries
        public async Task<IEnumerable<Company>> GetAllCompanies()
        {
            // Returns all companies
            var companies = await _context.Companies.ToListAsync();
            return companies;
        }
        public async Task<Company> GetCompanyByOrgNumber(int orgNumber)
        {
            // Returns a single company
            var company = await _context.Companies.SingleAsync(c => c.OrgNumber == orgNumber);
            return company;
        }
        // Checks if company exists
        public async Task<bool> CompanyExists(int orgNumber)
        {
            return await _context.Companies.AnyAsync(c => c.OrgNumber == orgNumber);
        }

        // Application queries
        public async Task<IEnumerable<ApplicationRM>> GetAllApplications()
        {
            // Gets a list with all applications
            var applications = await _context.Applications.Where(a => a.Deleted == false).ToListAsync();
            List<ApplicationRM> applicationRMs = new List<ApplicationRM>();
            // Converts from an application-object to an applicationRM-object
            foreach (var a in applications)
            {
                string json = JsonConvert.SerializeObject(a);
                ApplicationRM arm = JsonConvert.DeserializeObject<ApplicationRM>(json);
                
                // Fills the orgNumber and KYCId into the application
                arm = await ApplicationFill(arm, arm.CaseNumber);
                applicationRMs.Add(arm);
            }
            return applicationRMs;
        }
        public async Task<ApplicationRM> GetApplicationByCaseNumber(int caseNumber)
        {
            var application = await _context.Applications.Where(a => a.Deleted == false).SingleAsync(a => a.CaseNumber == caseNumber);
            string json = JsonConvert.SerializeObject(application);
            ApplicationRM applicationRM = JsonConvert.DeserializeObject<ApplicationRM>(json);

            // Fills the orgNumber and KYCId into the application
            applicationRM = await ApplicationFill(applicationRM, caseNumber);

            return applicationRM;
        }
        // Gets all applications that belong to a company
        public async Task<IEnumerable<ApplicationRM>> GetAllCompanyApplications(int orgNumber)
        {
            var allApplications = await _context.Applications.Where(a => a.Company.OrgNumber == orgNumber) .ToListAsync();
            var applications = allApplications.Where(a => a.Deleted == false);
            List<ApplicationRM> applicationRMs = new List<ApplicationRM>();
            foreach(var a in applications)
            {
                string json = JsonConvert.SerializeObject(a);
                ApplicationRM arm = JsonConvert.DeserializeObject<ApplicationRM>(json);
                arm = await ApplicationFill(arm, arm.CaseNumber);
                applicationRMs.Add(arm);
            }
            return applicationRMs;
        }
        // Get single application that belongs to a company
        public async Task<ApplicationRM> GetCompanyApplication(int orgNumber, int caseNumber)
        {
            // Finds the application with the given caseNumber and orgNumber
            var application = await _context.Applications.Where(a => a.Company.OrgNumber == orgNumber).SingleAsync(a => a.CaseNumber == caseNumber);
            // Converts the application-object to json
            string json = JsonConvert.SerializeObject(application);
            // Deserialize json to a applicationRM(ReadModel)
            // This is done to show an orgNumber instead of company = null
            ApplicationRM applicationRM = JsonConvert.DeserializeObject<ApplicationRM>(json);

            // Fills the orgNumber and KYCId into the application
            applicationRM = await ApplicationFill(applicationRM, caseNumber);

            return applicationRM;
        }
        public async Task<bool> ApplicationExistst(int caseNumber)
        {
            return await _context.Applications.Where(a => a.Deleted == false).AnyAsync(a => a.CaseNumber == caseNumber);
        }
        public async Task<bool> CompanyApplicationExists(int orgNumber, int caseNumber)
        {
            if(await _context.Applications.Where(a => a.Deleted == false).AnyAsync(a => a.CaseNumber == caseNumber))
            {
                ApplicationRM application = await GetApplicationByCaseNumber(caseNumber);
                if (application.CompanyOrgNumber == orgNumber)
                {
                    return true;
                }
            }
            return false;
        }
        // Fills the orgNumber and KYCId into the application
        private async Task<ApplicationRM> ApplicationFill(ApplicationRM applicationRM, int caseNumber)
        {
            // Gets orgNumber for an application
            var orgNumber = await _context.Applications.Where(a => a.CaseNumber == caseNumber).Select(a => a.Company.OrgNumber).SingleAsync();
            applicationRM.CompanyOrgNumber = orgNumber;

            // Gets KYCid for an application
            var kycid = await _context.Applications.Where(a => a.CaseNumber == caseNumber).Select(a => a.KYC.KYCId).SingleAsync();
            applicationRM.KYCid = kycid;

            // Gets the username of the casehandler for an application
            var handlerUsername = await _context.Applications.Where(a => a.CaseNumber == caseNumber).Select(a => a.CaseHandler.Username).SingleAsync();
            applicationRM.CaseHandlerUsername = handlerUsername;
            return applicationRM;
        }

        // CaseHandler queries
        public async Task<CaseHandler> GetCaseHandlerByUsername(string username)
        {
            var caseHandler = await _context.CaseHandlers.SingleAsync(ch => ch.Username == username);
            return caseHandler;
        }

        public async Task<bool> UsernameExists(string username)
        {
            return await _context.CaseHandlers.AnyAsync(ch => ch.Username == username);
        }

        // KYC queries
        public async Task<KYCRM> GetKYC(int caseNumber)
        {
            var kycid = await _context.Applications.Where(a => a.CaseNumber == caseNumber).Select(a => a.KYC.KYCId).SingleAsync();
            var kyc = await _context.KYCs.SingleAsync(k => k.KYCId == kycid);

            string json = JsonConvert.SerializeObject(kyc);

            KYCRM kycRM = JsonConvert.DeserializeObject<KYCRM>(json);

            var orgNumber = await _context.KYCs.Where(k => k.KYCId == kycid).Select(k => k.Company.OrgNumber).SingleAsync();
            kycRM.CompanyOrgNumber = orgNumber;

            return kycRM;
        }

        public async Task<IEnumerable<KYCRM>> GetKYCs(int orgNumber)
        {
            var kycs  = await _context.KYCs.Where(k => k.Company.OrgNumber == orgNumber).ToListAsync();

            List<KYCRM> kycrms = new List<KYCRM>();

            // Converts from KYC to KYCRM
            // This is to show the correct orgNumber instead of "company = null"
            foreach (KYC kyc in kycs)
            {
                string json = JsonConvert.SerializeObject(kyc);
                KYCRM kycrm = JsonConvert.DeserializeObject<KYCRM>(json);
                kycrm.CompanyOrgNumber = orgNumber;
                kycrms.Add(kycrm);
            }
            return kycrms;
        }
    }
}
