﻿using Aprila.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Aprila.CQRS.Queries
{
    public interface IQueriesService
    {
        // Company queries -------------------------------------------------------
        // GET all companies
        Task<IEnumerable<Company>> GetAllCompanies();
        // GET single company using orgNumber
        Task<Company> GetCompanyByOrgNumber(int orgNumber);
        Task<bool> CompanyExists(int orgNumber);

        // Application queries ---------------------------------------------------
        // GET all applications
        Task<IEnumerable<ApplicationRM>> GetAllApplications();
        // Get single application using caseNumber
        Task<ApplicationRM> GetApplicationByCaseNumber(int caseNumber);
        // GET all applications for a single company
        Task<IEnumerable<ApplicationRM>> GetAllCompanyApplications(int orgNumber);
        // Get single application for a company using orgNumber and caseNumber
        Task<ApplicationRM> GetCompanyApplication(int orgNumber, int caseNumber);
        Task<bool> ApplicationExistst(int caseNumber);
        Task<bool> CompanyApplicationExists(int orgNumber, int caseNumber);

        // CaseHandler queries ---------------------------------------------------
        // GET single casehandler using username 
        Task<CaseHandler> GetCaseHandlerByUsername(string username);
        Task<bool> UsernameExists(string username);

        // KYC queries -----------------------------------------------------------
        // GET single kyc with casenumber
        Task<KYCRM> GetKYC(int caseNumber);
        // Get all kycs for a company
        Task<IEnumerable<KYCRM>> GetKYCs(int orgNumber);
    }
}
