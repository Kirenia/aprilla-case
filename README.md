# Introduction 
TODO: Give a short introduction of your project. Let this section explain the objectives or the motivation behind this project. 

# API endpoints
## [ GET ]
### Companies
https://localhost:44399/api/companies

https://localhost:44399/api/companies/{OrgNumber}

### Applications
https://localhost:44399/api/applications

https://localhost:44399/api/applications/{casenumber}

https://localhost:44399/api/companies/{OrgNumber}/applications

https://localhost:44399/api/companies/{OrgNumber}/applications/{casenumber}

### Case Handler
https://localhost:44399/api/casehandler/{username}

### KYC
https://localhost:44399/api/companies/{OrgNumber}/kyc

https://localhost:44399/api/companies/{OrgNumber}/applications/{caseNumber}/kyc

## [ POST ]
### Companies
https://localhost:44399/api/companies

{
    "orgNumber": {int},
    "orgName": "{string}" 
}
* Every company has a "bankrupt" field, but this is set to false if not specified in the POST call

### Applications
https://localhost:44399/api/companies/{orgNumber}/applications

* Should not directly create a new application,
* a new application is created when a new KYC is created.
* When posting an application all you need is to make a post call to the url
* The code will then give it an id and set the date to when the application was created
* active will be set to "true" and accepted will be set to "false"
* comment and internalNote will be set to emtpy strings

### Case Handler
https://localhost:44399/api/casehandler

{
    "firstname": "{string}",
    "lastname": "{string}",
    "username": "{string}",
    "password": "{string}"
}

* The database can not contain duplicate usernames

### KYC
https://localhost:44399/api/companies/{OrgNumber}/kyc

{
	"ContactPerson":"{string}",
	"Email": "{string}",
	"Phone": "{string}",
	"SignatureRight": {bool},
	"BelongNorway": {bool},
	"Country": "{string}",
	"SelectedCountry": "{string}",
	"AccountNumber": "{string}",
	"Purpose": "{string}",
	"MonthlyTurnover": {UInt64},
	"pep": {bool}
}

* A new application is created when a new KYC is created
* The application and the KYC will be connected

## [ PUT ]
### Companies
https://localhost:44399/api/companies/{orgNumber}

{
    "orgName": "{string}"
}

https://localhost:44399/api/companies/{orgNumber}/bankrupt

### Applications
https://localhost:44399/api/companies/{orgNumber}/applications/{caseNumber}/comment

{
   "comment": "{string}"
}

https://localhost:44399/api/companies/{orgNumber}/applications/{caseNumber}/internalnote

{
   "internalnote": "{string}"
}

https://localhost:44399/api/companies/{orgNumber}/applications/{caseNumber}/accept

https://localhost:44399/api/companies/{orgNumber}/applications/{caseNumber}inactive

https://localhost:44399/api/applications/{casenumber}/casehandler

{
	"username": "{string}"
}

## [ DELETE ]
### Applications
https://localhost:44399/api/companies/{orgNumber}/applications/{caseNumber}

* An application will not show in queries if it is set to deleted

# Getting Started
TODO: Guide users through getting your code up and running on their own system. In this section you can talk about:
1.	Installation process
2.	Software dependencies
3.	Latest releases
4.	API references

# Build and Test
TODO: Describe and show how to build your code and run the tests.

## Building the solution
1. First you need to right click the solution and click "Set StartUp Projects...".
2. Select "Multiple startup projects".
3. Set the action of "Aprila.API" to "Start without debugging" and set the action of "AprilaCase" to "Start".
4. Apply these changes.
5. Now click "F5" or the green "run" button. All the necessary projects will now start and the solution will be running properly.

# Contribute
TODO: Explain how other users and developers can contribute to make your code better. 

If you want to learn more about creating good readme files then refer the following [guidelines](https://docs.microsoft.com/en-us/azure/devops/repos/git/create-a-readme?view=azure-devops). You can also seek inspiration from the below readme files:
- [ASP.NET Core](https://github.com/aspnet/Home)
- [Visual Studio Code](https://github.com/Microsoft/vscode)
- [Chakra Core](https://github.com/Microsoft/ChakraCore)
